# sunny

A control voltage and gate processor for Eurorack and other 1V/Oct systems.

## Language

The `sunny` control language is based on the concept of channels.
Each channel consists of one CV (analog) output and
one gate (digital) output.

Each track has an associated octave value, by default 4, in which it will
play note values.

### Example

```text
channel 0
bpm 120
legato on
octave 2

play c
play d
play e
channel 1 { set nF5; trigger }
off; wait beat

play c
play d
play e
channel 1 { set nG5; trigger }
off; wait beat

play c
play d
channel 1 { set nF5; trigger }
play e
channel 1 { set nG5; trigger }
off; wait beat

play b
off; wait beat
```

### Values

- Note values, either **simple** like `C`, `A#`, and `Db`,
**relative** like `C=` (C in this octave), `D+` (D in the next octave),
or `A-` (A one octave down),
or **absolute** like like `C2`, `Ab4`, or `F#0`.
Where a note value is needed, a voltage value (as below) can be substituted, prefixed with a `v`, like `v12c`.

- Voltage values, which can be expressed in volts (like `2v`), millivolts
(like `2330mv`), cents (like `220c`), semitones (like `4s`), or octaves (like `1o`), and which can be **absolute** or **relative** (like `+2c`).
Where a voltage value is needed, a note value (as above) can be substituted, prefixed with an `n`, like `n2C#`.

- Conf(iguration) values, which are used to set up channel parameters,
are either integers or real numbers (as context dictates) which can be
**absolute** or **relative**, and are sometimes **optional**, allowing the values "off" and "none".

- Boolean values, which can be `true` (or `on`) or `false` (or `off`).

- Time values, which are either `s`econds or `u` (milliseconds), or the special value `beat` which refers to one beat based on the BPM of the channel.

### Building Blocks

- `set <volts>` (abbr. `s`) sets the CV value of the channel to a given value.
This value can be given in `c`ents, `s`emitones (which is the default),
`o`ctaves, or 

- `play <note>` (abbr. `n`) sets the CV value to a given value and turns on and off the digital output for one beat minus the trigger amount (or simply leaves it on if legato is configured).

- `wait <time>` (abbr. `w`) makes the program wait until the next value.
Without the use of wait or play, all changes are executed simultaneously and
the output will simply be the last values set on the channel.

- `off` (abbr. `o`) turns off the digital channel.

- `trigger` (abbr. `t`, `trg`, `trig`) turns on the digital channel for a
very short time, then turns it off.

- `gate` (abbr. `g`, `gte`) turns on the digital channel until it is turned off again.

### Channel Control

- `channel <int> [{ <commands> }]` (abbr. `c`) sets the channel currently being controlled, or executes the given commands on the given channel.

- `octave <int>` (abbr. `v`) sets the octave, between 0 and 8, to which simple and relative note values will be played.

- `bpm <natural>` sets the tempo of the channel. This is the time used by the time value `beat` and by the `play` instruction.

- `portamento <optional natural>` turns on or off portamento (a slide between CV values) and sets its slope.

- `legato <boolean>` turns on or off legato. When legato is on, the `play` instruction will not set the gate output to zero at the end of the note.
